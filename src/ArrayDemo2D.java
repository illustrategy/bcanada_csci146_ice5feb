
public class ArrayDemo2D {

	public static void main(String[] args) {
		
		int[][] array = new int[3][3];
		
		// initialize the array elements
		for ( int row = 0; row < 3; row++ )
		{
			for ( int col = 0; col < 3; col++ )
			{
				array[row][col] = (row+1)*(col+1); 
						
			} // end inner for loop (for the columns of the current row)
			
		} // end outer for loop (for rows)
		
		
		// traverse and display the array element
		System.out.println(" \t0\t1\t2");
		
		// array.length is the number of rows
		for ( int i = 0; i < array.length; i++)
		{
			// print row heading for this row
			System.out.print( i + "\t" );
			
			// array[i].length is the number of columns (for row i)
			for ( int j = 0; j < array[i].length; j++ )
			{
				System.out.print( array[i][j] + "\t" );
				
			} // end inner for loop
			
			System.out.println(); // start newline for next row
			
		} // end outer loop
		
	} // end method main

} // end class ArrayDemo2D
